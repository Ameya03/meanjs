'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Contactlist = mongoose.model('Contactlist'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  contactlist;

/**
 * Contactlist routes tests
 */
describe('Contactlist CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Contactlist
    user.save(function () {
      contactlist = {
        name: 'Contactlist name'
      };

      done();
    });
  });

  it('should be able to save a Contactlist if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Contactlist
        agent.post('/api/contactlists')
          .send(contactlist)
          .expect(200)
          .end(function (contactlistSaveErr, contactlistSaveRes) {
            // Handle Contactlist save error
            if (contactlistSaveErr) {
              return done(contactlistSaveErr);
            }

            // Get a list of Contactlists
            agent.get('/api/contactlists')
              .end(function (contactlistsGetErr, contactlistsGetRes) {
                // Handle Contactlists save error
                if (contactlistsGetErr) {
                  return done(contactlistsGetErr);
                }

                // Get Contactlists list
                var contactlists = contactlistsGetRes.body;

                // Set assertions
                (contactlists[0].user._id).should.equal(userId);
                (contactlists[0].name).should.match('Contactlist name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Contactlist if not logged in', function (done) {
    agent.post('/api/contactlists')
      .send(contactlist)
      .expect(403)
      .end(function (contactlistSaveErr, contactlistSaveRes) {
        // Call the assertion callback
        done(contactlistSaveErr);
      });
  });

  it('should not be able to save an Contactlist if no name is provided', function (done) {
    // Invalidate name field
    contactlist.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Contactlist
        agent.post('/api/contactlists')
          .send(contactlist)
          .expect(400)
          .end(function (contactlistSaveErr, contactlistSaveRes) {
            // Set message assertion
            (contactlistSaveRes.body.message).should.match('Please fill Contactlist name');

            // Handle Contactlist save error
            done(contactlistSaveErr);
          });
      });
  });

  it('should be able to update an Contactlist if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Contactlist
        agent.post('/api/contactlists')
          .send(contactlist)
          .expect(200)
          .end(function (contactlistSaveErr, contactlistSaveRes) {
            // Handle Contactlist save error
            if (contactlistSaveErr) {
              return done(contactlistSaveErr);
            }

            // Update Contactlist name
            contactlist.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Contactlist
            agent.put('/api/contactlists/' + contactlistSaveRes.body._id)
              .send(contactlist)
              .expect(200)
              .end(function (contactlistUpdateErr, contactlistUpdateRes) {
                // Handle Contactlist update error
                if (contactlistUpdateErr) {
                  return done(contactlistUpdateErr);
                }

                // Set assertions
                (contactlistUpdateRes.body._id).should.equal(contactlistSaveRes.body._id);
                (contactlistUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Contactlists if not signed in', function (done) {
    // Create new Contactlist model instance
    var contactlistObj = new Contactlist(contactlist);

    // Save the contactlist
    contactlistObj.save(function () {
      // Request Contactlists
      request(app).get('/api/contactlists')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Contactlist if not signed in', function (done) {
    // Create new Contactlist model instance
    var contactlistObj = new Contactlist(contactlist);

    // Save the Contactlist
    contactlistObj.save(function () {
      request(app).get('/api/contactlists/' + contactlistObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', contactlist.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Contactlist with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/contactlists/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Contactlist is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Contactlist which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Contactlist
    request(app).get('/api/contactlists/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Contactlist with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Contactlist if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Contactlist
        agent.post('/api/contactlists')
          .send(contactlist)
          .expect(200)
          .end(function (contactlistSaveErr, contactlistSaveRes) {
            // Handle Contactlist save error
            if (contactlistSaveErr) {
              return done(contactlistSaveErr);
            }

            // Delete an existing Contactlist
            agent.delete('/api/contactlists/' + contactlistSaveRes.body._id)
              .send(contactlist)
              .expect(200)
              .end(function (contactlistDeleteErr, contactlistDeleteRes) {
                // Handle contactlist error error
                if (contactlistDeleteErr) {
                  return done(contactlistDeleteErr);
                }

                // Set assertions
                (contactlistDeleteRes.body._id).should.equal(contactlistSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Contactlist if not signed in', function (done) {
    // Set Contactlist user
    contactlist.user = user;

    // Create new Contactlist model instance
    var contactlistObj = new Contactlist(contactlist);

    // Save the Contactlist
    contactlistObj.save(function () {
      // Try deleting Contactlist
      request(app).delete('/api/contactlists/' + contactlistObj._id)
        .expect(403)
        .end(function (contactlistDeleteErr, contactlistDeleteRes) {
          // Set message assertion
          (contactlistDeleteRes.body.message).should.match('User is not authorized');

          // Handle Contactlist error error
          done(contactlistDeleteErr);
        });

    });
  });

  it('should be able to get a single Contactlist that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Contactlist
          agent.post('/api/contactlists')
            .send(contactlist)
            .expect(200)
            .end(function (contactlistSaveErr, contactlistSaveRes) {
              // Handle Contactlist save error
              if (contactlistSaveErr) {
                return done(contactlistSaveErr);
              }

              // Set assertions on new Contactlist
              (contactlistSaveRes.body.name).should.equal(contactlist.name);
              should.exist(contactlistSaveRes.body.user);
              should.equal(contactlistSaveRes.body.user._id, orphanId);

              // force the Contactlist to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Contactlist
                    agent.get('/api/contactlists/' + contactlistSaveRes.body._id)
                      .expect(200)
                      .end(function (contactlistInfoErr, contactlistInfoRes) {
                        // Handle Contactlist error
                        if (contactlistInfoErr) {
                          return done(contactlistInfoErr);
                        }

                        // Set assertions
                        (contactlistInfoRes.body._id).should.equal(contactlistSaveRes.body._id);
                        (contactlistInfoRes.body.name).should.equal(contactlist.name);
                        should.equal(contactlistInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Contactlist.remove().exec(done);
    });
  });
});
