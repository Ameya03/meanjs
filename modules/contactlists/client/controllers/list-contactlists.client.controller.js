(function () {
  'use strict';

  angular
    .module('contactlists')
    .controller('ContactlistsListController', ContactlistsListController);

  ContactlistsListController.$inject = ['ContactlistsService'];

  function ContactlistsListController(ContactlistsService) {
    var vm = this;

    vm.contactlists = ContactlistsService.query();
  }
}());
