'use strict';

/**
 * Module dependencies
 */
var contactlistsPolicy = require('../policies/contactlists.server.policy'),
  contactlists = require('../controllers/contactlists.server.controller');

module.exports = function(app) {
  // Contactlists Routes
  app.route('/api/contactlists').all(contactlistsPolicy.isAllowed)
    .get(contactlists.list)
    .post(contactlists.create);

  app.route('/api/contactlists/:contactlistId').all(contactlistsPolicy.isAllowed)
    .get(contactlists.read)
    .put(contactlists.update)
    .delete(contactlists.delete);

  // Finish by binding the Contactlist middleware
  app.param('contactlistId', contactlists.contactlistByID);
};
