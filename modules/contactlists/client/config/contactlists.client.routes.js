(function () {
  'use strict';

  angular
    .module('contactlists')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('contactlists', {
        abstract: true,
        url: '/contactlists',
        template: '<ui-view/>'
      })
      .state('contactlists.list', {
        url: '',
        templateUrl: 'modules/contactlists/views/list-contactlists.client.view.html',
        controller: 'ContactlistsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Contactlists List'
        }
      })
      .state('contactlists.create', {
        url: '/create',
        templateUrl: 'modules/contactlists/views/form-contactlist.client.view.html',
        controller: 'ContactlistsController',
        controllerAs: 'vm',
        resolve: {
          contactlistResolve: newContactlist
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Contactlists Create'
        }
      })
      .state('contactlists.edit', {
        url: '/:contactlistId/edit',
        templateUrl: 'modules/contactlists/views/form-contactlist.client.view.html',
        controller: 'ContactlistsController',
        controllerAs: 'vm',
        resolve: {
          contactlistResolve: getContactlist
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Contactlist {{ contactlistResolve.name }}'
        }
      })
      .state('contactlists.view', {
        url: '/:contactlistId',
        templateUrl: 'modules/contactlists/views/view-contactlist.client.view.html',
        controller: 'ContactlistsController',
        controllerAs: 'vm',
        resolve: {
          contactlistResolve: getContactlist
        },
        data: {
          pageTitle: 'Contactlist {{ contactlistResolve.name }}'
        }
      });
  }

  getContactlist.$inject = ['$stateParams', 'ContactlistsService'];

  function getContactlist($stateParams, ContactlistsService) {
    return ContactlistsService.get({
      contactlistId: $stateParams.contactlistId
    }).$promise;
  }

  newContactlist.$inject = ['ContactlistsService'];

  function newContactlist(ContactlistsService) {
    return new ContactlistsService();
  }
}());
