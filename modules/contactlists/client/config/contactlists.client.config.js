(function () {
  'use strict';

  angular
    .module('contactlists')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Contact Lists',
      state: 'contactlists',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'contactlists', {
      title: 'List Contact Lists',
      state: 'contactlists.list'
      
    });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'contactlists', {
      title: 'Add Contact',
      state: 'contactlists.create',
      roles: ['user']
    });
  }
}());
