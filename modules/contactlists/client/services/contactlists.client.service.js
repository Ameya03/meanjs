// Contactlists service used to communicate Contactlists REST endpoints
(function () {
  'use strict';

  angular
    .module('contactlists')
    .factory('ContactlistsService', ContactlistsService);

  ContactlistsService.$inject = ['$resource'];

  function ContactlistsService($resource) {
    return $resource('api/contactlists/:contactlistId', {
      contactlistId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
