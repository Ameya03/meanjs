(function () {
  'use strict';

  // Contactlists controller
  angular
    .module('contactlists')
    .controller('ContactlistsController', ContactlistsController);

  ContactlistsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'contactlistResolve'];

  function ContactlistsController ($scope, $state, $window, Authentication, contactlist) {
    var vm = this;

    vm.authentication = Authentication;
    vm.contactlist = contactlist;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Contactlist
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.contactlist.$remove($state.go('contactlists.list'));
      }
    }

    // Save Contactlist
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.contactlistForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.contactlist._id) {
        vm.contactlist.$update(successCallback, errorCallback);
      } else {
        vm.contactlist.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('contactlists.view', {
          contactlistId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
