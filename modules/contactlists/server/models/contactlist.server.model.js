'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Contactlist Schema
 */
var ContactlistSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Contact List name',
    trim: true
  },
  email: {
    type: String,
    default: '',
    required: 'Please fill Contact List email',
    trim: true
  },
  number: {
    type: String,
    default: '',
    required: 'Please fill Contact List number ',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Contactlist', ContactlistSchema);
