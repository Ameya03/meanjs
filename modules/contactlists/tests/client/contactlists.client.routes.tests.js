(function () {
  'use strict';

  describe('Contactlists Route Tests', function () {
    // Initialize global variables
    var $scope,
      ContactlistsService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _ContactlistsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      ContactlistsService = _ContactlistsService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('contactlists');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/contactlists');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          ContactlistsController,
          mockContactlist;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('contactlists.view');
          $templateCache.put('modules/contactlists/client/views/view-contactlist.client.view.html', '');

          // create mock Contactlist
          mockContactlist = new ContactlistsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Contactlist Name'
          });

          // Initialize Controller
          ContactlistsController = $controller('ContactlistsController as vm', {
            $scope: $scope,
            contactlistResolve: mockContactlist
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:contactlistId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.contactlistResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            contactlistId: 1
          })).toEqual('/contactlists/1');
        }));

        it('should attach an Contactlist to the controller scope', function () {
          expect($scope.vm.contactlist._id).toBe(mockContactlist._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/contactlists/client/views/view-contactlist.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          ContactlistsController,
          mockContactlist;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('contactlists.create');
          $templateCache.put('modules/contactlists/client/views/form-contactlist.client.view.html', '');

          // create mock Contactlist
          mockContactlist = new ContactlistsService();

          // Initialize Controller
          ContactlistsController = $controller('ContactlistsController as vm', {
            $scope: $scope,
            contactlistResolve: mockContactlist
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.contactlistResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/contactlists/create');
        }));

        it('should attach an Contactlist to the controller scope', function () {
          expect($scope.vm.contactlist._id).toBe(mockContactlist._id);
          expect($scope.vm.contactlist._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/contactlists/client/views/form-contactlist.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          ContactlistsController,
          mockContactlist;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('contactlists.edit');
          $templateCache.put('modules/contactlists/client/views/form-contactlist.client.view.html', '');

          // create mock Contactlist
          mockContactlist = new ContactlistsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Contactlist Name'
          });

          // Initialize Controller
          ContactlistsController = $controller('ContactlistsController as vm', {
            $scope: $scope,
            contactlistResolve: mockContactlist
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:contactlistId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.contactlistResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            contactlistId: 1
          })).toEqual('/contactlists/1/edit');
        }));

        it('should attach an Contactlist to the controller scope', function () {
          expect($scope.vm.contactlist._id).toBe(mockContactlist._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/contactlists/client/views/form-contactlist.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
