'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Contactlist = mongoose.model('Contactlist'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Contactlist
 */
exports.create = function(req, res) {
  var contactlist = new Contactlist(req.body);
  contactlist.user = req.user;

  contactlist.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(contactlist);
    }
  });
};

/**
 * Show the current Contactlist
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var contactlist = req.contactlist ? req.contactlist.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  contactlist.isCurrentUserOwner = req.user && contactlist.user && contactlist.user._id.toString() === req.user._id.toString();

  res.jsonp(contactlist);
};

/**
 * Update a Contactlist
 */
exports.update = function(req, res) {
  var contactlist = req.contactlist;

  contactlist = _.extend(contactlist, req.body);

  contactlist.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(contactlist);
    }
  });
};

/**
 * Delete an Contactlist
 */
exports.delete = function(req, res) {
  var contactlist = req.contactlist;

  contactlist.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(contactlist);
    }
  });
};

/**
 * List of Contactlists
 */
exports.list = function(req, res) {
  Contactlist.find().sort('-created').populate('user', 'displayName').exec(function(err, contactlists) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(contactlists);
    }
  });
};

/**
 * Contactlist middleware
 */
exports.contactlistByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Contactlist is invalid'
    });
  }

  Contactlist.findById(id).populate('user', 'displayName').exec(function (err, contactlist) {
    if (err) {
      return next(err);
    } else if (!contactlist) {
      return res.status(404).send({
        message: 'No Contactlist with that identifier has been found'
      });
    }
    req.contactlist = contactlist;
    next();
  });
};
